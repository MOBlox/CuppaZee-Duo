if(!global.clans) global.clans = {};
if(!global.clanreload) global.clanreload = {};
if(!global.clanpromise) global.clanpromise = {};
if(!global.clanresolved) global.clanresolved = {};
if(!global.users) global.users = {};
if(!global.usercache) global.usercache = {};
if(!global.ucad) global.ucad = {};
global.resetData = function(){
	global.clans = {};
	global.clanreload = {};
	global.clanpromise = {};
	global.clanresolved = {};
	global.users = {};
	global.usercache = {};
	global.ucad = {};
}

global.a = 0;

global.timezone, global.hqT, global.doffset = 0, global.gmtOffset = 0;
global.gTZ = async function(){
	//Get Timezone
	timezone = JSON.parse(await rp('https://api.timezonedb.com/v2/get-time-zone?key=S0T3SKWNPU73&format=json&by=position&lat=33.214561&lng=-96.614456'));
	doffset = (timezone.gmtOffset * 1000) + ((new Date()).getTimezoneOffset() * 60000);
	gmtOffset = timezone.gmtOffset * 1000;
	console.log('gottimezoneinfo');
}
gTZ();
global.hqStr = function(){
	var tm = hqT();
	let month = String(tm.getMonth() + 1);
	let date = String(tm.getDate());
	if(month.length == 1){
		month = "0" + month;
	}
	if(date.length == 1){
		date = "0" + date;
	}
	return `${tm.getFullYear()}-${month}-${date}`;
}
global.hqT = function(){return new Date(Date.now() + doffset);};
global.dayStart = function(){
	var xxx = new Date(Date.now())
	xxx.setHours(0,-new Date(Date.now()).getTimezoneOffset(),0,-gmtOffset);
	if(xxx.valueOf() > new Date(Date.now()).valueOf()) xxx = new Date(xxx.valueOf() - (24 * 60 * 60 * 1000));
	return xxx;
}

global.getUName = async function(id, doc){ // Get Username of User
	var auth = doc ? doc.access_token : await getAT();
	var options = {
		method: 'POST',
		uri: 'https://api.munzee.com/user/',
		headers: {
			'Authorization': auth, 
		},
		formData: {
			data: `{"user_id":${id}}`, 
		}
	};
	var body = await rp(options);
	var data = JSON.parse(body).data;
	return data.username;
}

global.getAT = async function(id, expired){ //Get User Access Token
    var doc, xxx;
    if(id && typeof id == "number"){
        xxx = access.doc(getUName(id));
    } else if(id){
        xxx = access.doc(getUName(id));
    } else {
        xxx = access.doc('sohcah');
	}
	doc = SOP(await xxx.get());
	if(!doc) return false;
	var newdoc = doc;
    if((Date.now() + 60000) / 1000 > newdoc.expires || expired){
		try {
			var options = {
				method: 'POST',
				uri: 'https://api.munzee.com/oauth/login',
				formData: {
					'client_id': config.client_id,
					'client_secret': config.client_secret,
					'grant_type': 'refresh_token',
					'refresh_token': doc.refresh_token, 
					'redirect_uri': config.redirect_uri
				}
			};
			Object.assign(newdoc, JSON.parse(await rp(options)).data.token);
			newdoc.username = await getUName(newdoc.user_id, newdoc);
			await xxx.set(newdoc);
		} catch(e) {
			// console.log(doc.username);
			if(e.toString().includes('The refresh token is invalid.')){
				console.log('REMOVED', e.toString());
				await xxx.remove();
			}
		}
    }
    return newdoc.access_token;
}

global.getUName = async function(id, doc){
	var auth = doc ? doc.token.access_token : await getAT();
	var options = {
		method: 'POST',
		uri: 'https://api.munzee.com/user/',
		headers: {
			'Authorization': auth, 
		},
		formData: {
			data: `{"user_id":${id}}`, 
		}
	};
	var body = await rp(options);
	var data = JSON.parse(body).data;
	return data.username;
}

global.mR = async function(page, data, id, expired){ // Munzee Request
	var auth = id ? id : await getAT();
	if(!auth) console.log('No auth?');
	var options = {
		method: 'POST',
		uri: encodeURI('https://api.munzee.com/' + page),
		headers: {
			Authorization: encodeURI(auth),
		},
		formData: {
			data: JSON.stringify(data), 
		}
	};
	// console.log("OPTIONS:", options);
	try {
		var raw = await rp(options)
		var body = JSON.parse(raw).data;
		return body;
	} catch(e) {
		// console.log('Unexpected error occurred', e);
		//discordMsg('000', await access.where('access_token', '==', auth).get());
		discordMsg('111',e);
		var uid = SOP(await (access.where('access_token', '==', auth)).get()).user_id;
		if(!expired) {
			return await mR(page, data, await getAT(uid), true);
		} else {
			return Error('Unable to get data');
		}
	}
}
global.postTS = async function(){
	rp({
		method: 'POST',
		uri: 'http://ptsv2.com/t/1bgtz-1536163241/post',
		formData: {
			username: 'CuppaZee Notification',
			content: JSON.stringify(arguments)
		}
	})
		.catch(console.log);
}
global.discordMsg = async function(){
	let ret = JSON.stringify(arguments);
	for(var i = 0;i < ret.length / 1900 && i < 10;i++){
		rp({
			method: 'POST',
			uri: 'https://ptb.discordapp.com/api/webhooks/476077608078147598/CoMntIEMXfuB_CrGfH6BOfmVvOdnCsE336JoZ5a-_jPVpcjlX6JlG3B4x_d6ij_ZLiBC',
			formData: {
				username: 'CuppaZee Notification',
				content: i + "```" + ret.slice(i*1900,(i+1)*1900) + "```"
			}
		})
			.catch(console.log);
	}
	
}
global.cleanCapDep = function(a){
	return a.map(i=>{
		return i.icon.replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/(?:v4)?pins\/(?:svg\/)?(.+)\..+/,'$1')
	});
}
global.betterZeeQRew = async function(u){
	var zq = await zeeQRew(u);
	var ip = await rp(`https://munzee.com/m/${u}`);
	var out = {
		handle: '',
		premium: false,
		level: 0,
		points: 0,
		physical_captures: 0,
		physical_deploys: 0
	}
	out.handle = ip.match(/<div class="avatar-username"><a href="\/m\/[^\/]+\/">([^<]+)<\/a><\/div>/)[1];
	if(ip.includes('class="fa fa-star"')) out.premium = true;
	out.points = Number(ip.match(/<a href="https:\/\/statzee.munzee.com\/player\/points\/[^"]+">([0-9,]+)<\/span>/)[1].replace(/,/g,''));
	out.level = Number(ip.match(/<a href="#" data-toggle="tooltip" title="Current Level" class="user-rank tooltip-holder"><span class="badge badge-success">([0-9,]+)<\/span><\/a>/)[1].replace(/,/g,''));
	var virts = [];
	for(var i = 0;i < Object.keys(zq.c).length;i++){
		let key = Object.keys(zq.c)[i];
		let obj = zq.c[key];
		if(useful.phyVirt[key] == 1) out.physical_captures += obj;
		// MYTH STUFF HERE
	}
	for(var i = 0;i < Object.keys(zq.d).length;i++){
		let key = Object.keys(zq.d)[i];
		let obj = zq.d[key];
		if(useful.phyVirt[key] == 1) out.physical_deploys += obj;
		if(useful.phyVirt[key] == 1) virts.push(`${key}:${obj}`);
	}
	discordMsg(virts);
	return out;
}
global.zeeQRew = async function(u){
	//uSER, pAGE
    var uri = `https://statzee.munzee.com/player/captures/${u}`
	var j = rp.jar();
    var url = "https://statzee.munzee.com";
    var cookie = rp.cookie(config.userLogin);
    j.setCookie(cookie, url);
    var caps = await rp({uri, jar:j});
    var deps = await rp({uri: uri.replace('captures','deploys'), jar:j});
    var out = {c: {}, d: {}};
	caps = caps.replace(/\n/g,'').replace(/[^]+<div class="page-header summary"> <h3>Captures By Type<\/h3> <\/div> <table class="table table-hover" style="margin-top: 40px;"> <thead> <tr> <th><\/th> <th class="hidden-xs">Capture Type<\/th> <th>Captures<\/th> <th>Average Points<\/th> <th>Total Points<\/th> <th class="hidden-xs">Percentage<\/th> <\/tr> <\/thead> <tbody>([^]+)<\/tbody> <\/table> <\/div> <\/div> <\/div> <\/div>[^]+/,'$1').split('</tr> <tr>').map(i=>i.replace(/<\/?tr>/g,''));
	deps = deps.replace(/\n/g,'').replace(/[^]+<div class="page-header summary"> <h3>Deployed Munzees By Type<\/h3> <\/div> <table class="table table-hover" style="margin-top: 40px;"> <thead> <tr> <th><\/th> <th class="hidden-xs">Munzee Type<\/th> <th>Munzees<\/th> <th>Captures On<\/th> <th>Deploy Points<\/th> <th>Cap On Points<\/th> <th>Combined Points<\/th> <th class="hidden-xs">Percentage<\/th> <\/tr> <\/thead> <tbody>([^]+)<\/tbody> <\/table> <\/div> <\/div> <\/div> <\/div>[^]+/,'$1').split('</tr> <tr>').map(i=>i.replace(/<\/?tr>/g,''));
	for(var i = 0;i < caps.length-1;i++){
		let cap = caps[i];
		out.c[cap.match(/img src="([^"]+)"/)[1].replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?([^\.]+)\..+/,'$1')] = Number(cap.match(/<td class="hidden-xs"><i>[^<]+<\/i><\/td> ?<td>([^<]+)<\/td>/)[1].replace(/,/g,''));
	}
    for(var i = 0;i < deps.length-1;i++){
		let dep = deps[i];
		out.d[dep.match(/img src="([^"]+)"/)[1].replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?([^\.]+)\..+/,'$1')] = Number(dep.match(/<td class="hidden-xs"><i>[^<]+<\/i><\/td> ?<td>([^<]+)<\/td>/)[1].replace(/,/g,''));
	}
	return out;
}
global.capSpecials = async function(u){
	//uSER, pAGE
    var uri = `https://www.munzee.com/m/${u}/specials/`
    var j = rp.jar();
    var url = "https://www.munzee.com";
    var cookie = rp.cookie(config.userLogin);
    j.setCookie(cookie, url);
    var caps = await rp({uri, jar:j});
    var out = {};
	caps = caps.replace(/\n/g,'').replace(/[^]+<ul id="specials-listing" class="list-inline">[\n\s]+<li>[\n\s]+([^]+)[\n\s]+<\/li>[\n\s]+<\/ul>[^]+/,'$1').replace(/\n/g,'').split('</li>          <li>');
	for(var i = 0;i < caps.length-1;i++){
		let cap = caps[i];
		out[cap.match(/img src="([^"]+)"/)[1].replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?([^\.]+)\..+/,'$1')] = Number(cap.match(/<span class="badge">([^<]+)<\/span>/)[1].replace(/,/g,''));
	}
	return out;
}
global.newPlayerCapDep = async function(u,p,dep,special,debug){
	//uSER, pAGE
    var uri = `https://www.munzee.com/m/${u}/${dep ? 'deploys' : 'captures'}/${special ? "specials/"+special+"/" : ''}${p}/`
	var j = rp.jar();
    var url = "https://www.munzee.com";
    var cookie = rp.cookie(config.userLogin);
    j.setCookie(cookie, url);
    var caps = await rp({uri, jar:j}); //,jar:j
    
    if(debug)discordMsg(uru);
    var out = [];
    if(!caps.includes('munzee-holder')) discordMsg('err');
	caps = caps.replace(/\n/g,'').replace(/[^]+<div id="munzee-holder">([^]+)<\/div> <ul class="pager">[^]+/,'$1').split('<section>').map(i=>i.replace('</section>',''));
	if(debug)postTS(caps);
    for(var i = 0;i < caps.length-1;i++){
		let cap = caps[i+1];
		out[i] = {};
		if(!dep) out[i].captured_at = cap.match(/data-captured-at="([^"]+)"/)[1];
		out[i].deployed_at = cap.match(/data-deployed-at="([^"]+)"/)[1];
		out[i].icon = cap.match(/class="pin(?: hidden-xs)?" src="([^"]+)">/)[1];
		out[i].points = cap.match(/<div class="number">([^<]+)<\/div>/)[1];
		out[i].name = cap.match(/<a class="munzee-id" href="([^"]+)">([^<]+)<\/a>/)[2];
		out[i].url = "https://www.munzee.com" + cap.match(/<a class="munzee-id" href="([^"]+)">([^<]+)<\/a>/)[1];
		out[i].pin = out[i].icon.replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?(.+)\..+/,'$1');
		out[i].type = useful.types[out[i].icon.replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?(.+)\..+/,'$1')];
		if(typeof out[i].type == 'function'){out[i].type = out[i].type(out[i]);}
		if(!out[i].type) out[i].type = -1;
		out[i].capture_type_id = out[i].type;
	}
	return out;
}
global.GPA = async function(player){
	// GetPlayerActivity
	try {
		console.log('GPA1',player);
        var token = getAT(), ds = dayStart(), cap = [], dep = [], i, uid = player,tempucad = [];
        if(!usercache[uid] || !usercache[uid].cap || !usercache[uid].dep){
            usercache[uid] = {cap: [], dep: []};
            ucad[uid] = [];
        }
        if((usercache[uid].cap.length > 0 && (new Date(usercache[uid].cap[0].captured_at)).valueOf() < ds.valueOf()) || ((usercache[uid].dep.length > 0 && (new Date(usercache[uid].dep[0].captured_at)).valueOf() < ds.valueOf()))){
            usercache[uid] = {cap: [], dep: []};
            ucad[uid] = [];
		}
		console.log('GPA2',player);
		var q = await (capdb.doc(player)).get();
		if(q._fieldsProto){
			var DayStartCaps = SOP(q);
			var NowCaps = await capSpecials(player);
			for(var i = 0;i < Object.keys(NowCaps).length;i++){
				let key = Object.keys(NowCaps)[i];
				if(player == "sohcah") console.log(key)
				for(var j = 0;j < NowCaps[key] - (DayStartCaps[key] || 0);j++){
					cap.push({
						type: useful.types[key] || -1,
						pin: key
					});
					//if(!useful.types[key]) console.log('Missing new capture',key);
				}
			}
		} else {
			cap = "NoLiveData"
		}
		console.log('GPCaps',player);

		/*
        for(var sp = 0;sp < useful.specials.length;sp++){
            for(i = 0;i < 100;i++){
                //let dt = await mR('user/captures',{user_id:uid, page: i});
                let dt = await newPlayerCapDep(player,i,false,useful.specials[sp]);
                //if(i == 0 && player == 'sohcah') discordMsg('Capture',dt);
                // if(uid == 125932){
                // 	postTS('GunnerSteve DT', i, dt);
                // }
                //discordMsg('capdt', dt[0]);
                if(dt.length==0) break;
                for(let j = 0;j < dt.length;j++){
                    if((new Date(dt[j].captured_at)).valueOf() >= ds.valueOf() && !ucad[uid].includes(AID(uid,dt[j],'C'))){ // && !(usercache[uid].cap.length > 0 && dt[j].captured_at == usercache[uid].cap[usercache[uid].cap.length - 1].captured_at && dt[j].munzee_id == usercache[uid].cap[usercache[uid].cap.length - 1].munzee_id)
                        if(!tempucad.includes(AID(uid,dt[j],'C'))){
                            cap.push(dt[j]);
                            tempucad.push(AID(uid,dt[j],'C'));
                        }
                    } else {
                        //discordMsg('EndedGettingCaptures', player, dt[j], ds.valueOf())
                        i = 10000;
                        break;
                    }
                }
            }
        }*/
        for(i = 0;i < 100;i++){
            //let dt = (await mR('user/deploys',{user_id:uid, page: i})).munzees;
            let dt = await newPlayerCapDep(player,i,true);
            //discordMsg('depdt', dt[0]);
            for(let j = 0;j < dt.length;j++){
                if((new Date(dt[j].deployed_at)).valueOf() >= ds.valueOf() && !ucad[uid].includes(AID(uid,dt[j],'D'))){ // && !(usercache[uid].dep.length > 0 && dt[j].deployed_at == usercache[uid].dep[usercache[uid].dep.length - 1].deployed_at && dt[j].munzee_id == usercache[uid].dep[usercache[uid].dep.length - 1].munzee_id)
                    dep.push(dt[j]);
                    tempucad.push(AID(uid,dt[j],'D'));
                } else {
                    i = 10000;
                    break;
                }
            }
		}/*
        for(i = 0;i < cap.length;i++){
            usercache[uid].cap.push(cap[i]);
        }*/
        for(i = 0;i < tempucad.length;i++){
            ucad[uid].push(tempucad[i]);
        }
        for(i = 0;i < dep.length;i++){
            usercache[uid].dep.push(dep[i]);
        }
		console.log('GotPA',player);
        return {dep: usercache[uid].dep, cap: cap}/* usercache[uid].cap */
	} catch(e) {
		discordMsg(e.toString());
	}
}
global.SOP = function(s){
	// Store OutPut
	var fields = s._fieldsProto;
	var ret = {};
	Object.keys(fields).map(ii=>{
		i = fields[ii];
		var val = i[i.valueType];
		if(i.valueType == "integerValue"){
			ret[ii] = Number(val);
		} else {
			ret[ii] = val;
		}
	});
	return ret;
};
global.AID = function(uid, x,w){
	// ActivityID
	return w + (w == 'C' ? "|" + x.captured_at : '') + "|" + x.name + "|" + uid + "|" + x.icon + "|" + x.deployed_at + "|" + x.points;
}
global.getClanData = async function(clanid){
	//if(clanid !== 1441 && clanid !== 0) return;
	try {
		console.log('Reloading Clan', clanid);
		clanreload[clanid] = Date.now();

		// Clan Live Data? \/
		var q = await capdb.doc('XXXXX').get();
		var x = SOP(q).clans.values.map(i=>i.stringValue);
		if(x.map(i=>i.split('|')[0]).includes(clanid.toString())){
			x[x.map(i=>i.split('|')[0]).indexOf(clanid.toString())] = `${clanid}|${hqStr()}`;
		} else {
			x.push(`${clanid}|${hqStr()}`);
		}
		await capdb.doc('XXXXX').set({clans:x});
		// Clan Live Data? /\

		console.log('Reloading Clan', clanid);
		var munzees = Object.assign({},useful.munzees);
		var months = ['January','Febuary','March','April','May','June','July','August','September','October','November','December','January'];
		
		var clan = [],timenow = hqT(), token = await getAT(),requ = useful.requirements[timenow.getMonth()],reqls = [],allreq = ['tot_p']; //'cap_p','dep_p','con_p',
		if(useful.requirements[timenow.getMonth() + 1] && clanid == -1) requ = useful.requirements[timenow.getMonth() + 1];
		Object.keys(requ.clan[4]).map((item)=>{reqls.push({t: "clan",i: item})});
		Object.keys(requ.user[4]).map((item)=>{reqls.push({t: "user",i: item})});
		reqls.map((item)=>{if(!allreq.includes(item.i))allreq.push(item.i)});
		if(clanid <= 0){
			// Requirements \/
			var r = {name: 'other.requirements', id: 0, tables: {}};
			var tables = [
				`{"val":"other.requirements"}`,
				`{"val":"other.rewards"}`,
				`{"val":"other.next_month"}`,
				`{"val":"other.next_month_rewards"}`
			];
			for(var i = 0;i < 4;i++){
				let table;
				if(i == 1 || i == 3){
					if(useful.requirements[(timenow.getMonth()+((i-1)/2)) % 12]){
						table = [[{val:'other.level',lvl:-3,reqval:'other.level',reqimg:'none'}],[{val:{val:'other.level_n',args:{level: 1}},lvl:1}],[{val:{val:'other.level_n',args:{level: 2}},lvl:2}],[{val:{val:'other.level_n',args:{level: 3}},lvl:3}],[{val:{val:'other.level_n',args:{level: 4}},lvl:4}],[{val:{val:'other.level_n',args:{level: 5}},lvl:5}]];
						let r = useful.requirements[(timenow.getMonth()+((i-1)/2)) % 12].rewards;
						var x = Object.keys(Object.assign({},r[0],r[1],r[2],r[3],r[4]))
						for(var l = 0;l < x.length;l++){
							let k = x[l];
							table[0].push({val:'',reqval:'other.reward',reqimg:`https://munzee.global.ssl.fastly.net/images/v4pins/${k}.png`,lvl:-3});
							for(var j = 0;j < 5;j++){
								table[j+1].push({lvl:j+1,val:r[j][k] || 0});
							}
						}
					}
				} else {
					if(useful.requirements[(timenow.getMonth()+(i/2)) % 12]){
						let rqls = [];
						let req = useful.requirements[timenow.getMonth()+(i/2) % 12];
						table = [[{val: req.name,lvl:-3},{val: 'reqType.clan',colspan: Object.keys(req.clan[4]).length,lvl:-3},{val: 'reqType.user',colspan: Object.keys(req.user[4]).length,lvl:-3}],[{val:'other.level',lvl:-3,reqval:'other.level',reqimg:'none'}],[{val:{val:'other.level_n',args:{level: 1}},lvl:1}],[{val:{val:'other.level_n',args:{level: 2}},lvl:2}],[{val:{val:'other.level_n',args:{level: 3}},lvl:3}],[{val:{val:'other.level_n',args:{level: 4}},lvl:4}],[{val:{val:'other.level_n',args:{level: 5}},lvl:5}]];
						// table[0].push({val: 'Clan', desc: 'Clan Total Requirements'});
						for(var rq of Object.keys(req.clan[4])){
							let reqdesc = useful.reqdescs[rq];
							reqdesc.args.reqType = 'reqType.clan';
							table[1].push({val: '' + useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: reqdesc,lvl:-3, reqimg: useful.reqimgs[rq][1]});
							rqls.push('clan_' + rq);
						}
						// table[0].push({val: 'User', desc: 'User Requirements'});
						for(var rq of Object.keys(req.user[4])){
							let reqdesc = useful.reqdescs[rq];
							reqdesc.args.reqType = 'reqType.clan';
							table[1].push({val: '' + useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: reqdesc,lvl:-3, reqimg: useful.reqimgs[rq][1]});
							rqls.push('user_' + rq);
						}

						for(var l = 0;l < 5;l++){
							// table[l+1].push({val: '', lvl: -1});
							for(var rq in req.clan[l]){
								table[l+2][rqls.indexOf('clan_'+rq)+1] = {val: req.clan[l][rq] || '---', bold: !!req.clan[l][rq], lvl: l+1};
							}
							// table[l+1].push({val: '', lvl: -1});
							for(var rq in req.user[l]){
								table[l+2][rqls.indexOf('user_'+rq)+1] = {val: req.user[l][rq] || '---', bold: !!req.user[l][rq], lvl: l+1};
							}
						}
					}
				}
				r.tables[tables[i]] = table;
			}
			r.timestamp = Date.now();
			//r.hoverText = `Reloaded at ${hqT().getHours()}:${hqT().getMinutes()} MHQ`;
			r.hoverText = {
				val: 'other.reloaded_at',
				args: {
					time: `${hqT().getHours()}:${hqT().getMinutes() < 10 ? '0' : ''}${hqT().getMinutes()}`
				}
			}
			clanreload[clanid] = false;
			return r;
			// Requirements /\
		}
		// Clan Stats
		var basicData = {};
		var clanLiveDataBlock = false;
		clan[0] = await mR('clan/',{"clan_id":clanid},token);
		clan[1] = await mR('clan/stats',{"clan_id":clanid},token);
		if(clan[1] && clanid == 1349 && timenow.getDate() == 4){
			discordMsg('CLANDATA', Object.keys(clan[1]));
		}
		console.log('Got Data', clanid);
		if(timenow.getDate() < 3){
			var ret = {name: clan[0].details.name,
				id: clanid, 
				tables: {
					'{"val":"other.summary"}':[
						[
							{val: ''},{val: `Members (${clan[0].users.length}) - C&D Indicators Not Available`, bold: true}
						]
					]
				},
				timestamp: Date.now()
			}
			for(var i = 0;i < clan[0].users.length;i++){
				ret.tables['{"val":"other.summary"}'][i+1] = [{val: ''},{val: clan[0].users[i].username}];
			}
			clanreload[clanid] = false;
			return ret;
		}
		
		// Basic Data \/
		for(var i = 0;i < clan[0].users.length;i++){
			basicData[clan[0].users[i].username] = {};
			for(let j = 0;j < allreq.length;j++){
				let rqfrm = useful.reqfrom[allreq[j]];
				if(rqfrm[0] == "d"){
					basicData[clan[0].users[i].username][allreq[j]] = clan[0].users[i][rqfrm[1]] || 0;
				} else {
					if(clan[1] && clan[1][rqfrm[1]]){
						basicData[clan[0].users[i].username][allreq[j]] = clan[1][rqfrm[1]][clan[0].users[i].username] || 0;
					} else {
						basicData[clan[0].users[i].username][allreq[j]] = 0;
					}
				}
			}
			let ud = await GPA(clan[0].users[i].username);
			function inc(req,amount){
				if(allreq.includes(req)) basicData[clan[0].users[i].username][req] += amount || 1;
			}
			if(ud.banned){
				basicData[clan[0].users[i].username].banned = true;
			}
			var capStreak = await mR('statzee/player/captures',{username:clan[0].users[i].username});
			if(ud.cap == "NoLiveData"){
				clanLiveDataBlock = true;
			} else {
				var cappedtoday = false;
				for(let j = 0;j < ud.cap.length;j++){
					let cap = ud.cap[j];
					let p = cap.pin;
					basicData[clan[0].users[i].username].cap_day = basicData[clan[0].users[i].username].cap_day || 'red';
					if(!p.includes('social')) {
						basicData[clan[0].users[i].username].cap_day = true;
						cappedtoday = true;
					}
					/*let md = useful.munzees[Number(cap.type).toString()];
					inc('cap');
					if(md){
						// May 18
						//#if(md.gaming) inc('gam_cap');
			
						// June 18
						//#if(md.physical) inc('phy_cap');
						//#if(md.physical && (md.mythhost || md.alternahost || md.pouchhost)) inc('phy_cap');
						//#if(md.myth || md.alterna) inc('myth_cap');
			
						// July 18
			
						// August 18
						//#if(md.id != "social") inc('cap_xsoc');	

						// September 18
						//#if(md.id == 'mystery') inc('mys_cap');
						//#if(['motel','hotel','motel-room','hotel-room'].includes(md.id)) inc('mothot_cap');
						//#if(md.physical && md.jewel) inc('phyjew_cap');
						//#if(md.physical && md.evolution) inc('phyevo_cap');
						//#if(md.id == 'premium') inc('prem_cap');

						// October 18
						if(md.id == "temp-virtual") inc('tempvirt_cap');
						if(md.id == "event-indicator") inc('event_cap');
						if(md.retiremyth) inc('rmyth_cap');
						if(md.virtual && md.evolution) inc('virtevo_cap');
						if(md.id == "surprise") inc('surprise_cap');
						if(md.id == "scatter") inc('scatter_cap');
						if(md.id == "scattered") inc('scattered_cap');
						if(md.id == "prize-wheel") inc('prize_cap');
					}*/
					//if(['temporaryvirtual'].includes(p)) inc('tempvirt_cap');
					//if(['eventindicator'].includes(p)) inc('event_cap');
					//if(p.startsWith('retired')) inc('rmyth_cap');
					//if(['carrotseed','carrotplant','carrot','peasseed','peasplant','peas','colt','racehorse','championshiphorse','chick','chicken','eggs','farmer','farmerandwife','family','pottedplant','garden','field','canoe','motorboat','submarine','firstwheel','penny-farthingbike','musclecar','safaritruck','safarivan','safaribus'].includes(p)) inc('virtevo_cap');
					//if(['surprise'].includes(p)) inc('surprise_cap');
					//if(['scatter'].includes(p)) inc('scatter_cap');
					//if(['scattered'].includes(p)) inc('scattered_cap');
					//if(['prizewheel'].includes(p)) inc('prize_cap');
					//if(['unicorn','leprechaun','hydra','cyclops','pegasus','faun','yeti','dragon','battle_unicorn','hippocamp_unicorn','mermaid','fairy','dryad_fairy','wildfire_fairy','rainbowunicorn','icedragon','gnomeleprechaun','sasquatchyeti','firepegasus','cherub','ogre','chimera','siren'].includes(p)) inc('myth_cap');
					if(['diamond','ruby','topaz','virtualamethyst','virtual_amethyst','pinkdiamond','aquamarinemunzee','virtualsapphire','virtual_sapphire','virtualemerald'].includes(p)) inc('nov18_jewel_cap')
					if(p.startsWith('poi')) inc('nov18_places_cap');
					if(['mace','longsword','battleaxe','thehammer','crossbow','catapult'].includes(p)) inc('nov18_clan_cap');
					if((p === 'virtual' || p.startsWith('virtual_')) && p !== 'virtual_amethyst') inc('nov18_virtual_cap')
					if(['aquarius','pisces','aries','taurus','gemini','cancer','leo','virgo','libra','scorpio','sagittarius','capricorn'].includes(p) || p.includes('zodiac')) inc('nov18_zodiac_cap');
					if(p === 'leprechaun') inc('nov18_leprechaun_cap');
					if(p.includes('nomad')) inc('nov18_nomad_cap');
					if(p === 'yeti') inc('nov18_yeti_cap');
					if(p === 'faun') inc('nov18_faun_cap');
					if(p.startsWith('retired')) inc('nov18_retmyth_cap');
					if(p === 'mermaid') inc('nov18_mermaid_cap');
					if(p === 'hydra') inc('nov18_hydra_cap');
					if(p.includes('tuli') || p.includes('vesi') || p.includes('muru')) inc('nov18_pouch_cap');
					

					// "nov18_cap_streak": ["s","N/A"],
				}
				var longestStreak = 0;
				var currentStreak = 0;
				for(var s = 3;s < timenow.getDate();s++){
					if(capStreak[hqStr().slice(0,-2) + (s < 10 ? '0' : '') + s] > 0){
						currentStreak += 1;
						longestStreak = Math.max(longestStreak, currentStreak);
					} else {
						currentStreak = 0;
					}
				}
				if(cappedtoday){
					currentStreak += 1;
					longestStreak = Math.max(longestStreak, currentStreak);
				}
				basicData[clan[0].users[i].username].nov18_cap_streak = longestStreak;
			}
			console.log('Got User Cap', clan[0].users[i].username);
			for(let j = 0;j < ud.dep.length;j++){
				let dep = ud.dep[j];
				let p = dep.pin;
				basicData[clan[0].users[i].username].dep_day = true;
				if((p === 'virtual' || p.startsWith('virtual_')) && p !== 'virtual_amethyst') inc('nov18_virtual_dep');
				if(p === 'munzee') inc('nov18_greenie_dep');
				if(['mace','longsword','battleaxe','thehammer','crossbow','catapult'].includes(p)) inc('nov18_clan_dep')
				if(['diamond','ruby','topaz','virtualamethyst','virtual_amethyst','pinkdiamond','aquamarinemunzee','virtualsapphire','virtual_sapphire','virtualemerald'].includes(p)) inc('nov18_jewel_dep');
				/*
				let md = useful.munzees[Number(dep.capture_type_id).toString()];
				inc('dep');
				if(md){
					// June 18
					if(md.physical) inc('phy_dep');
		
					// July 18
					if(md.clan) inc('clan_dep');
					if(md.jewel) inc('jewel_dep');

					basicData[clan[0].users[i].username].dep_day = true;
		
					// August 18
					if(md.id != "social") inc('dep_xsoc');

					// September 18
					if(md.id == 'mystery') inc('mys_dep');
					if(md.physical && md.jewel) inc('phyjew_dep');
					if(md.physical && md.evolution) inc('phyevo_dep');
					if(md.id == 'premium') inc('prem_dep');
					if(['motel','hotel','motel-room','hotel-room'].includes(md.id)) inc('mothot_dep');

					// October 18
					if(md.id == "scatter") inc('scatter_dep');
				}*/

				
			}
			console.log('Got User Dep', clan[0].users[i].username);
			if(!basicData[clan[0].users[i].username].cap_day){
				if(clan[0].users[i].capture_points != ( ( (clan[1] || {})['capture'] || {} )[clan[0].users[i].username] || 0 )){
					basicData[clan[0].users[i].username].cap_day = true;
				}
			}
			console.log('Got User BD', clan[0].users[i].username);
		}
		// Basic Data /\

		console.log('Got Basic Data', clanid);

		// To Tables \/

		var r = {};
		r.name = clan[0].details.name;
		r.rank = clan[0].ranking + "/" + clan[0].number_of_clans;
		r.id = clanid;
		r.tables = {};

		var users = Object.keys(basicData);
		users.sort(function(a,b){
		return basicData[b].tot_p - basicData[a].tot_p;
		});

		var tls = ['Main', '10%', 'Level 1', 'Level 2', 'Level 3', 'Level 4', 'Level 5'];
		tls = ['{"val":"other.summary"}','{"val":"10%"}','{"val":"other.level_n","args":{"level":1}}','{"val":"other.level_n","args":{"level":2}}','{"val":"other.level_n","args":{"level":3}}','{"val":"other.level_n","args":{"level":4}}','{"val":"other.level_n","args":{"level":5}}'];
		var maxlvl=5; 
		for (var tn = 0; tn < 7; tn++) {
			var coloring = i=>false;
			if(tn==0)coloring = i=>["tot_p","nov18_cap_streak","nov18_diff_places_cap","nov18_retmyth_cap","nov18_leprechaun_cap","nov18_yeti_cap","nov18_faun_cap","nov18_mermaid_cap","nov18_hydra_cap","nov18_clan_dep","nov18_jewel_dep","nov18_places_cap","nov18_clan_cap","nov18_virtual_cap","nov18_virtual_dep","nov18_pouch_cap"].includes(i);
			if(tn == 1)continue;
			let CDSep = 1; // 1 - No, 2 - Yes
			let table = r.tables[tls[tn]] = [[{ lvl: -1, val: 'other.players', reqimg: 'none' }]]; //, { lvl: -1, val: '', desc: 'Captured and Deployed Today?' }
			let un = 0; // User number
			let clanLvl = 5;
			let ul = Object.keys(basicData).length;
			let lrow = table[ul + 1] = [{ lvl: 5, val: 'reqType.clan' }]; //, {lvl: -1}
			for (let rq of allreq) {
				let reqdesc = useful.reqdescs[rq];
				reqdesc.args.reqType = '';
				table[0].push({ lvl: -1, val: useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: reqdesc, rawrq: rq, reqimg: useful.reqimgs[rq][1] }) 
			};
			table[0].push({ lvl: -1, val: 'other.level_short', desc: 'Level', reqimg: 'none' });
			let clanTotal = {};
			for (let name of users) {
				// For each Person
				un++;
				var userLvl = 5;
				if(!coloring('user_lvl')) userLvl = -2;
				var ud = basicData[name];//Userdata
				var row = table[un] = [{ lvl: 5, val: name, player: true, desc: 'Click to go to ' + name + '\'s Munzee Page' }, { lvl: -1 }];
				for (let rq in ud) {
					// rq = requirement
					let sc = ud[rq]; // Score
					if (rq == "cap_day") { row[CDSep - 1].cap = sc } else if (rq == "dep_day") { row[CDSep - 1].dep = sc } else if (rq == "banned") { row[CDSep - 1].banned = true; row[CDSep - 1].desc = 'Can\'t access ' + name + '\'s Munzee Data'; row[CDSep - 1].player = false; } else {
						if (tn < 2) {
							let lvl = 0;
							if (tn == 0) {
								for (lvl = 0; lvl < 5 && (requ.user[lvl][rq] || 0) <= sc; lvl++) { }
								if (!requ.user[4][rq] || !coloring(rq)) lvl = -2;
							} else {
								for (lvl = 0; lvl < 5 && Math.max(((requ.clan[lvl][rq] || 0) / 10), requ.user[lvl][rq] || 0) <= sc; lvl++) { }
								if ((!requ.clan[4][rq] && !requ.user[4][rq]) || !coloring(rq)) lvl = -2;
							}
							row[allreq.indexOf(rq) + CDSep] = { val: sc, lvl: lvl };
							if (lvl != -2) userLvl = Math.min(userLvl, lvl);
						} else {
							let lvl = tn - 2;
							if (requ.user[lvl][rq]) {
								row[allreq.indexOf(rq) + CDSep] = { val: Math.max(0, requ.user[lvl][rq] - sc), lvl: requ.user[lvl][rq] > sc ? 0 : lvl + 1 };
								userLvl = Math.min(requ.user[lvl][rq] > sc ? 0 : lvl + 1, userLvl);
							} else {
								row[allreq.indexOf(rq) + CDSep] = { val: 0, lvl: -2 };
							}
						}
						if(rq == 'nov18_cap_streak'){
							clanTotal[rq] = Math.min(clanTotal[rq]||Infinity, sc);
						} else {
							clanTotal[rq] = (clanTotal[rq] || 0) + sc;
						}
					}
				}
				if(userLvl == -2){
					clanLvl = -2;
				}
				if(Object.keys(requ.user[4]).length == 0 || userLvl == -2) userLvl = "---";
				row[0].lvl = userLvl == "---" ? -2 : userLvl;
				if(CDSep == 2) row[1].lvl = userLvl == "---" ? -2 : userLvl;
				row[row.length] = {lvl: userLvl == "---" ? -2 : userLvl, val: userLvl, bold: userLvl == "---", desc: `${name} is at Level ${userLvl}`};
				if(userLvl != "---") clanLvl = Math.min(clanLvl, userLvl);
			}
			for (let rq of allreq) {
				let sc = clanTotal[rq];
				if (tn < 2) {
					for (lvl = 0; lvl < 5 && (requ.clan[lvl][rq] || 0) <= sc; lvl++) { }
					if (!requ.clan[4][rq] || !coloring(rq)) lvl = -2;
					lrow[allreq.indexOf(rq) + CDSep] = { val: sc, lvl: lvl };
					if (lvl != -2) clanLvl = Math.min(clanLvl, lvl);
				} else {
					let lvl = tn - 2;
					if (requ.clan[lvl][rq]) {
						lrow[allreq.indexOf(rq) + CDSep] = { val: Math.max(0, requ.clan[lvl][rq] - sc), lvl: requ.clan[lvl][rq] > sc ? 0 : lvl + 1 };
						clanLvl = Math.min(clanLvl, requ.clan[lvl][rq] > sc ? 0 : lvl + 1);
					} else {
						lrow[allreq.indexOf(rq) + CDSep] = { val: 0, lvl: -2 };
					}
				}
			}

			lrow[0].lvl = clanLvl;
			if(CDSep == 2) lrow[1].lvl = clanLvl;
			if(clanLvl == -2) clanLvl = '---';
			lrow[lrow.length] = {lvl: clanLvl == "---" ? -2 : clanLvl, val: clanLvl, bold: clanLvl == "---", desc: `The clan is at Level ${clanLvl}`};
			
			// RECALCULATE USER LEVELS FOR 10%, AND CLAN LEVEL \/
			if(tn == 1){
				let i = 0;
				for(let name in basicData){
					i++;
					let ur = table[i];//User Row
					for(var j = CDSep;j < ur.length;j++){
						if(j != ur.length - 1){
							if(!requ.clan[4][table[0][j].rawrq] && !requ.user[4][table[0][j].rawrq]){
								table[i][j].lvl = -2;
							} else {
								// Requirement
								let rq = table[0][j].rawrq;
								let ob = table[i][j];
								let cl = table[ul+1][j];
								let lvl = Math.min(cl.lvl, 5);
								for (var xlvl = 0; xlvl < 5 && ((requ.clan[xlvl][rq] || 0) / 10) <= ob.val; xlvl++) { }
								lvl = Math.max(lvl, xlvl);
								if(r.tables[tls[0]][i][j].lvl != -2) lvl = Math.min(r.tables[tls[0]][i][j].lvl, lvl);
								ob.lvl = lvl;
							}
							
						}
					}
					table[i][0].lvl = r.tables[tls[0]][i][0].lvl;
					if(CDSep == 2) table[i][1].lvl = r.tables[tls[0]][i][0].lvl;
					table[i][table[i].length-1] = {val: r.tables[tls[0]][i][0].lvl, lvl: r.tables[tls[0]][i][0].lvl, desc: `${name} is at Level ${r.tables[tls[0]][i][0].lvl}`};
				}
				table[ul+1][0].lvl = r.tables[tls[0]][ul+1][0].lvl;
				if(CDSep == 2) table[ul+1][1].lvl = r.tables[tls[0]][ul+1][0].lvl;
				table[ul+1][table[ul+1].length-1] = {val: r.tables[tls[0]][ul+1][0].lvl, lvl: r.tables[tls[0]][ul+1][0].lvl, desc: `The clan is at Level ${r.tables[tls[0]][ul+1][0].lvl}`};
			}
			// RECALCULATE USER LEVELS FOR 10%, AND CLAN LEVEL /\
		}

		// To Tables /\

		// Return \/
		r.reloadExpires = Date.now() + 300000;
		r.timestamp = Date.now();
		r.dataBlock = clanLiveDataBlock;
		//r.hoverText = `Ranking: ${clan[0].ranking}/${clan[0].number_of_clans}<br class="d-sm-block d-md-none"><span class="d-none d-md-inline"> | </span>Reloaded at ${hqT().getHours()}:${hqT().getMinutes()} MHQ`;
		r.hoverText = {
			val: 'other.reloaded_and_ranking',
			args: {
				rank: clan[0].ranking,
				total: clan[0].number_of_clans,
				time: `${hqT().getHours()}:${hqT().getMinutes() < 10 ? '0' : ''}${hqT().getMinutes()}`,
				seperator: '<br class="d-sm-block d-md-none"><span class="d-none d-md-inline"> | </span>'
			}
		}
		clanreload[clanid] = false;
		console.log('Returning Clan', clanid);
		//console.log(clanid, r);
		return r;
		// Return /\
	} catch(e){
		discordMsg('Error getting Clan Stats', e.toString())
	}
}

global.loopFunction = async function(){
	a++;
	console.log('Loop',a);
	if(global.currentTime == Math.floor(Date.now()/60000)) return;
	if(hqT().getHours()+ ":" + hqT().getMinutes() == "0:0"){
		resetData();
		var q = await capdb.doc('XXXXX').get();
		var x = SOP(q).clans.values.map(i=>i.stringValue);
		for(var i = 0;i < x.length;i++){
			let v = x[i].split('|')[1].split('-');
			let c = Number(x[i].split('|')[0].replace(/[^0-9]/g,'')); //Clan
			if(c !== false && c !== undefined && c !== 0 && c !== NaN && c !== null){
				let users = (await mR('clan/',{"clan_id":c})).users.map(i=>i.username);
				console.log(c,users,v,(new Date(`${v[1]}/${v[2]}/${v[0]}`)).valueOf() + 1209600000 > hqT());
				if( (new Date(`${v[1]}/${v[2]}/${v[0]}`)).valueOf() + 1209600000 > hqT() ){
					for(var j = 0;j < users.length;j++){
						let u = users[j]; //User
						try {
							capdb.doc(u).set(await capSpecials(u));
						} catch(e){
							try {
								capdb.doc(u).remove();
							} catch(e){}
						}
					}
				} else {
					for(var j = 0;j < users.length;j++){
						let u = users[j]; //User
						try {
							capdb.doc(u).remove();
						} catch(e){}
					}
				}
			}
		}
	} else if(hqT().getMinutes() == 0){
		var q = await SS.doc(hqT().getHours().toString()).get();
		if(q._fieldsProto){
			var x = SOP(q).users.values.map(i=>i.stringValue);
			for(var i = 0;i < x.length;i++){
				let u = x[i].split('|');
				let username = u[0];
				let userid = u[1];
				let email = u[2];
				let id = u[3];
				let type = (u[4] || 'CD').split('');
				console.log(u);
				let data = await rp({
					method: 'POST',
					uri: 'https://www.munzee.com/ajax/user/indicator/',
					body: {
						"user_id": userid.toString()
					},
					json: true
				});
				console.log(data);
				data = data.match(new RegExp(`data-original-title="${['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'][hqT().getMonth()]} ${hqT().getDate()} \\| ([0-9]+) deployed \\| ([0-9]+) captured"`)).slice(1).map(Number);
				console.log(data);
				if((data[0] == 0 && type.includes('D')) || (data[1] == 0 && type.includes('C'))){
					transporter.sendMail({
						from: {
							name: 'StreakSaver',
							address: 'noreply@cuppazee.uk'
						},
						to: email,
						subject: 'Save your Munzee Streak!',
						text: `Hi ${username},\nYou\'ve not done your ${(data[1] == 0 && type.includes('C')) ? 'Capture' : ''}${(data[0] == 0 && type.includes('D')) && (data[1] == 0 && type.includes('C')) ? ' and ' : ''}${(data[0] == 0 && type.includes('D')) ? 'Deploy' : ''} today!\n\n\nUnsubscribe at https://api.cuppazee.uk/unsubscribe?s=${id}&t=${hqT().getHours()}`,
						html: `<p style="color: red;">Hi ${username},<br>You\'ve not done your ${(data[1] == 0 && type.includes('C')) ? 'Capture' : ''}${(data[0] == 0 && type.includes('D')) && (data[1] == 0 && type.includes('C')) ? ' and ' : ''}${(data[0] == 0 && type.includes('D')) ? 'Deploy' : ''} today!</p><a href="https://api.cuppazee.uk/unsubscribe?s=${id}&t=${hqT().getHours()}">Unsubscribe</a>`
					});
				}
			}
		}
	}
	global.currentTime = Math.floor(Date.now()/60000);
}

global.ClanRedirect = function(i){
	var list = {1144: 1402};
	if(list[i]) return list[i];
	return i;
}
global.getClanId = async function(e){
    if(/^\d+$/.test(e.replace('https','http').replace('http://stats.munzee.dk/?',''))) {
        if((await mR('clan/',{"clan_id":Number(e.replace('https','http').replace('http://stats.munzee.dk/?',''))})).users.length > 0) {
            return ClanRedirect(Number(e.replace('https','http').replace('http://stats.munzee.dk/?','')))
        }
    }
    var x = (await mR('clan/id/',{simple_name: e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase()})).clan_id;
    if(x != 0){
        return ClanRedirect(x);
    } else {
        var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
        var regexStatement = new RegExp('<ahref/clans/([^/]+)/>' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
        var matching = clanpage.match(regexStatement);
        if(matching){
            x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
            return ClanRedirect(x);
        } else {
            matching = null;
            var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
            var regexStatement = new RegExp('<ahref/clans/([^/]+)/>the' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
            matching = clanpage.match(regexStatement);
            if(matching){
                x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                return ClanRedirect(x);
            } else {
                matching = null;
                var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
                var regexStatement = new RegExp('<ahref/clans/([^/]+)/>' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + 'clan</a>')
                matching = clanpage.match(regexStatement);
                if(matching){
                    x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                    return ClanRedirect(x);
                } else {
                    matching = null;
                    var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
                    var regexStatement = new RegExp('<ahref/clans/([^/]+)/>the' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + 'clan</a>')
                    matching = clanpage.match(regexStatement);
                    if(matching){
                        x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                        return ClanRedirect(x);
                    } else {
                        console.log('CLAN NOT AVAILABLE', e);
                        return ClanRedirect(0);
                    }
                }
            }
        }
    }
}

global.reloadClan = async function(clan, force){
	console.log(clanpromise[clans],clan);
	if(clanpromise[clans] === null || clanpromise[clans] === undefined || clanresolved[clans] === true){ //!clanreload[clan] || clanreload[clan] < Date.now() - 60000
		if(!clans[clan] || !clans[clan].timestamp){
			//discordMsg('ClanReloading','NoTimeStamp', Date.now(),'NotAvailable or NoTimeStamp');
			clanpromise[clan] = getClanData(clan);
			clanresolved[clan] = false;
			clans[clan] = await clanpromise[clan];
			clanpromise[clan] = null;
			clanresolved[clan] = true;
		} else if(clans[clan].timestamp < Date.now() - (15 * 60000)){
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'15Mins');
			clanpromise[clan] = getClanData(clan);
			clanresolved[clan] = false;
			clans[clan] = await clanpromise[clan];
			clanpromise[clan] = null;
			clanresolved[clan] = true;
		} else if(force && clans[clan].timestamp < Date.now() - 60000){
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'Force1Min');
			clanpromise[clan] = getClanData(clan);
			clanresolved[clan] = false;
			clans[clan] = await clanpromise[clan];
			clanpromise[clan] = null;
			clanresolved[clan] = true;
		} else {
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'None',force ? 'Forced' : 'Not Forced');
		}
	} else {
		await clanpromise[clan];
	}
	return clans[clan];
}

// global.socketFunction = async function(socket){
// 	socket.on('requestclan',async function(e){
// 		reloadClan(e.id, e.force);
//         if(clans[e.id] && clans[e.id].tables) io.emit('clan',clans[e.id]);
// 	});
// 	socket.on('requestuser',async function(e){
// 		if(!users[e.id] || e.force){
// 			users[e.id] = await getUserData(e.id);
// 		}
// 		io.emit('user',{id: e.id, info: users[e.id]});
// 	});

// 	socket.on('getclanid',async function(e){
// 		let x = await getClanId(e)
// 		discordMsg('AddingClan',e,x);
// 		socket.emit('addclan',x);
// 	});
// };

global.pages = [
	{
		page: ['*','/reload'],
		function(req, res){
            reload();
            res.send('Reloading now. Thank you =)')
		}
	},{
		page: ['*','/auth'],
		function(req, res){
			if(req.query.code) {
				console.log(req.query.code);
				var options = {
					method: 'POST',
					uri: 'https://api.munzee.com/oauth/login',
					url: 'https://api.munzee.com/oauth/login',
					formData: {
						'client_id': config.client_id,
						'client_secret': config.client_secret,
						'grant_type': 'authorization_code',
						'code': req.query.code, 
						'redirect_uri': config.redirect_uri
					}
				};
				rp(options)
					.then(async function (body) {
						var data = JSON.parse(body).data;
						console.log(data);
						data.username = await getUName(data.user_id, data);
						access.doc(data.username).set({
							"access_token": data.token.access_token,
							"token_type": data.token.token_type,
							"expires": data.token.expires,
							"expires_in": data.token.expires_in,
							"refresh_token": data.token.refresh_token,
							"user_id": data.user_id,
							"username": data.username
						});
						discordMsg('UserAuthenticated',data.username);
						res.send(`<script>window.location.replace('https://cuppazee.uk/');</script>`);
					})
					.catch(async function (err) {
						res.send(`${err} - Error - Redirecting in 3 seconds<script>setTimeout(()=>{window.location.replace('https://zee.moblox.co.uk/')},3000);</script>`);
					});
			} else {
				res.send(`<script>window.location.replace('https://cuppazee.co.uk/');</script>`);
			}
		}
	},{
		page: ['api.cuppazee.uk','/page/get'],
		function(req, res){
			res.send(fs.readFileSync('page.html','utf8'));
		}
	},{
		page: ['api.cuppazee.uk','/page/alpha'],
		function(req, res){
			res.send(fs.readFileSync('alpha.html','utf8'));
		}
	},{
		page: ['api.cuppazee.uk','/clan/find'],
		async function(req, res){
			if(req.query.clan){
				res.send((await getClanId(req.query.clan)).toString());
			} else {
				res.send('Error - No Clan');
			}
		}
	},{
		page: ['api.cuppazee.uk','/clan/get'],
		function(req, res){
			if(req.query.id){
				if(clans[Number(req.query.id)]){
					res.send(JSON.stringify(clans[Number(req.query.id)]));
				} else {
					res.send('Error - Try Reload');
				}
			} else {
				res.send('Error - No ID');
			}
		}
	},{
		page: ['api.cuppazee.uk','/clan/tryreload'],
		async function(req, res){
			if(req.query.id){
				await reloadClan(Number(req.query.id));
				res.send(JSON.stringify(clans[Number(req.query.id)]));
			} else {
				res.send('Error - No ID');
			}
		}
	},{
		page: ['api.cuppazee.uk','/clan/forcereload'],
		async function(req, res){
			if(req.query.id){
				await reloadClan(Number(req.query.id), true);
				res.send(JSON.stringify(clans[Number(req.query.id)]));
			} else {
				res.send('Error - No ID');
			}
		}
	},{
		page: ['api.cuppazee.uk','/subscribe'],
		async function(req, res){
			if(req.query.t && req.query.e && req.query.u && req.query.cd){
				var q = await SS.doc(req.query.t).get();
				if(q._fieldsProto){
					var x = SOP(q).users.values.map(i=>i.stringValue);
					let userid = (await mR('/user/',{username: req.query.u})).user_id;
					let id = [...Array(16)].map(i=>(~~(Math.random()*36)).toString(36)).join('');
					x.push(`${req.query.u}|${userid}|${req.query.e}|${id}|${req.query.cd}`);
					SS.doc(req.query.t).set({
						users: x
					});
					transporter.sendMail({
						from: {
							name: 'StreakSaver',
							address: 'noreply@cuppazee.uk'
						},
						to: req.query.e,
						subject: 'Successfully Signed Up',
						text: `Hi ${req.query.u},\nThank you for Signing up to StreakSaver Emails\nDisclaimer: We take no responsibility if you lose your streak.\n\n\nUnsubscribe at https://api.cuppazee.uk/unsubscribe?s=${id}&t=${req.query.t}`,
						html: `<p>Hi ${req.query.u},<br>Thank you for Signing up to StreakSaver Emails</p><p style="color: grey;">Disclaimer: We take no responsibility if you lose your streak.</p><a href="https://api.cuppazee.uk/unsubscribe?s=${id}&t=${req.query.t}">Unsubscribe</a>`
					});
				}
				res.send('Subscribed.');
			} else {
				res.send('Error - No ID');
			}
		}
	},{
		page: ['api.cuppazee.uk','/unsubscribe'],
		async function(req, res){
			if(req.query.s && req.query.t){
				var q = await SS.doc(req.query.t).get();
				if(q._fieldsProto){
					var x = SOP(q).users.values.map(i=>i.stringValue);
					for(var i = 0;i < x.length;i++){
						let u = x[i].split('|');
						if(u[3] == req.query.s){
							x.splice(i,1);
						}
					}
					SS.doc(req.query.t).set({
						users: x
					});
				}
				res.send('Unsubscribed.');
			} else {
				res.send('Error - Missing Parameters');
			}
		}
	},{
		page: ['api.cuppazee.uk','/postexec'],
		async function(req, res){
			if(req.body.pw == config.password){
				res.send(JSON.stringify([await eval(req.body.c || null)]));
			} else {
				res.send('No Perms');
			}
		}
	},{
		page: ['api.cuppazee.uk','/greenie/api'],
		async function(req, res){
			if(req.body.pw == config.greeniePassword){
				if(req.body.apipage && req.body.apibody){
					res.send(JSON.stringify(await mR(req.body.apipage, JSON.parse(req.body.apibody))));
				} else {
					res.send('Error - Missing Args');
				}
			} else {
				res.send('No Perms');
			}
		}
	},
	{
		page: ['api.cuppazee.uk','/translations'],
		function(req, res){
			res.send(fs.readFileSync('translationdocs.html','utf8'));
		}
	}
]
