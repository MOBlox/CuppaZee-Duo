(async function(){
    var express = require('express');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var Firestore = require('@google-cloud/firestore');
    var nodemailer = require('nodemailer');
    var rp = require('request-promise');
	/*var rp = async function(o){
		console.log('Request',o);
		return await reqp(o);
	}*/
    var fs = require('fs');
    global.reload = async function(){
        global.useful = JSON.parse(fs.readFileSync('useful.json','utf8'));
        global.page = fs.readFileSync('page.html','utf8');
        global.config = JSON.parse(fs.readFileSync('config.json','utf8'));
        eval(await rp('https://gitlab.com/MOBlox/CuppaZee-2/raw/master/img2type.js?inline=false'));
        useful.munzees = JSON.parse(await rp('https://gitlab.com/MOBlox/CuppaZee-2/raw/master/munzees.json?inline=false'));
        eval(fs.readFileSync('main.js','utf8'));
    }
    await reload();
    
    

    var db = new Firestore({
      projectId: 'cuppazeestats-us',
      keyFilename: 'keyfile.json',
      timestampsInSnapshots:true
    });
    var access = db.collection('access');
    var capdb = db.collection('cap');
    var SS = db.collection('streaksaver');

    var transporter = nodemailer.createTransport({
        host: 'smtp.zoho.eu',
        port: 465,
        secure: true,
        auth: {
                user:'noreply@cuppazee.uk',
                pass:config.password
        }
    });
    // transporter.sendMail({
    //     from: {
    //         name: 'StreakSaver',
    //         address: 'noreply@cuppazee.uk'
    //     },
    //     to: 'samstars28@gmail.com',
    //     subject: 'Save your Munzee Streak!',
    //     text: 'You\'ve not done your Capture and Deploy today!\n\n\nUnsubscribe at https://api.cuppazee.uk/unsubscribe?s=1234567890qwerty',
    //     html: '<p style="color: red;">You\'ve not done your Capture and Deploy today!</p><a href="https://api.cuppazee.uk/unsubscribe?s=1234567890qwerty">Unsubscribe</a>'
    // });
    
    var app = express();
    app.use(cookieParser());
    app.use(express.static('public'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.enable('trust proxy');
    
    app.all(/.+/, async function(req, res){
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        var nopage = true;
        for(var i = 0; i < pages.length && nopage; i++){
            if(( pages[i].page[1] == req.path || pages[i].page[1] == "*") && ( pages[i].page[0] == req.hostname || pages[i].page[0] == "*")){
                nopage = false;
                pages[i].function(req, res);
            }
        };
        if(nopage){
            res.send('404 - Page not found');
        }
    });
    
    setInterval(()=>{
        loopFunction();
    }, 30000);
    
    app.listen(5332).on('error', function(err) { console.log(err) });
    console.log('Listening');
})();
    

