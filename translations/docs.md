# CuppaZee Translations
CuppaZee now has support for translations.
If you are interested in converting CuppaZee, you can contact us (See More tab on CuppaZee Website for Contact info).

For information on how to write Translation files, see below.

*Translation file format is subject to change. If the format does change, MOBlox will change the formatting of all translation files*  
*Any un-translated text will be shown in English. Most months we will have to add more to the translation files.*

## Info
CuppaZee's translation files are written in [JSON](https://json.org/).

Some translation texts have inputs inside. You can see an example here:  
`"Hello, {username}. It is {time}"`.

For larger texts (Long bits of text on the more tab, etc.), we have additional formatting options, as we support [Markdown](https://daringfireball.net/projects/markdown/syntax).  
Additionally, these texts have 2 additional inputs, which are as follows:  
`{nl}` - Puts in a new line  
`{np}` - Makes a new paragraph

## English Translation File
```json
{
    "lang_name": "English",
    "ui": {
        "tabs": {
            "clans": "Clans",
            "news": "News",
            "more": "More"
        },
        "add_clan": {
            "add_clan": "Add Clan",
            "add_a_clan": "Add a Clan",
            "clan_name": "Clan Name",
            "info": "If the clan you add hasn't been loaded in 14 days, live data will be unavailable for it until midnight MHQ time.{nl}A clan without live data will show this following icon next to its name: {icon}"
        },
        "news": {
            "flats_calendar": "Flats Calendar",
            "latest_news": "Latest News",
            "days": {
                "monday": "M",
                "tuesday": "T",
                "wednesday": "W",
                "thursday": "T",
                "friday": "F",
                "saturday": "S",
                "sunday": "S"
            }
        },
        "more": {
            "info": {
                "title": "Info",
                "body": "**What is CuppaZee?**{nl}CuppaZee is a tool to support Clan Wars in the Geolocation game [Munzee](//munzee.com){nl}If you want to start playing Munzee you can scan a Referral Code, which will give you, and the referrer, some extra points.{np}**Credits**{nl}[Thegenie18](//munzee.com/m/thegenie18) for the suggestion to make this project{nl}[Boompa](//munzee.com/m/boompa) for a load of suggestions for the clan stats.{nl}[Stebu](//munzee.com/m/stebu) for Finnish translations.{nl}All the Cup of [Tea](//munzee.com/clans/thecupofteaclan), [Coffee](//munzee.com/clans/thecupofcoffeeclan) and [Cocoa](//munzee.com/clans/thecupofcocoaclan) Clan members for giving feedback and testing.{np}CuppaZee Version {version}. © {copyrightDate} MOBlox"
            },
            "installing": {
                "title": "Install CuppaZee",
                "body": "**Installing on Android**{nl}After using CuppaZee for a while on your Android device, you should get a banner asking whether you want to install CuppaZee. You can then press the button to install.{np}**Installing on iOS or Android**{nl}Press Share (on iOS) or Menu (on Android), then press \"Add to Homescreen\" to add CuppaZee to your Homescreen."
            },
            "contact": {
                "title": "Contact Us",
                "body": "You can contact us:{nl}By **Email** at [mail@cuppazee.uk](mailto:mail@cuppazee.uk){nl}By **Discord** using the #CuppaZee channel on the [Munzee Discord by Technical13](https://discord.me/greenie){nl}By **Facebook** on the [CuppaZee Page](//fb.me/CuppaZee){nl}By **FB Messenger** at [CuppaZee](//m.me/CuppaZee){nl}By **Twitter** [@CuppaZeeUK](//twitter.com/CuppaZeeUK)"
            },
            "tools": {
                "title": "Other Tools",
                "body": "[MunzStat Web](//munzstat.com) - Destination Planner, BounceInator™ and more!{nl}[MunzStat App](//play.google.com/store/apps/details?id=dk.lksoft.munzstat) - An easy overview of Daily Activity, Leaderboards, Creatures and loads of stats.{nl}[Eeznum](//eeznum.co.uk) - Lots of fun and interesting stats, including lots for Bouncing Creatures.{nl}[Munzee.dk](//munzee.dk) - Social Capper, Official Munzee Retailer, Creature Stats and more."
            },
            "social": {
                "title": "CuppaZee Social Munzee",
                "body": "Feel free to cap this free social for using CuppaZee!{nl}![CuppaZee Social Munzee](https://image.ibb.co/i4P6nf/cuppasocial.png)"
            },
            "streaksaver": {
                "title": "StreakSaver",
                "body": {
                    "description": "StreakSaver is an email service you can signup for, which will send you an email at a certain time of day if you haven't done your streak for that day.",
                    "email": {
                        "title": "Email Address",
                        "placeholder": "Enter email",
                        "smallText": "We'll never share your email with anyone else."
                    },
                    "username": {
                        "title": "Username",
                        "placeholder": "Munzee Username",
                        "smallText": ""
                    },
                    "time": {
                        "title": "Reminder Time (MHQ Time)",
                        "options": [
                            "1am",
                            "2am",
                            "3am",
                            "4am",
                            "5am",
                            "6am",
                            "7am",
                            "8am",
                            "9am",
                            "10am",
                            "11am",
                            "12pm",
                            "1pm",
                            "2pm",
                            "3pm",
                            "4pm",
                            "5pm",
                            "6pm",
                            "7pm",
                            "8pm",
                            "9pm",
                            "10pm",
                            "11pm"
                        ]
                    },
                    "type": {
                        "title": "Streak Type",
                        "options": {
                            "both": "Capture and Deploy Streak",
                            "cap": "Capture Streak Only",
                            "dep": "Deploy Streak Only"
                        }
                    },
                    "signup": "Sign up",
                    "success": "**Successfully Subscribed to StreakSaver emails!**{nl}You should receive an email confirming your signup shortly."
                }
            }
        }
    },
    "requirement": {
        "number_of": "{reqType} Number of {munzee} {action}",
        "length": "{reqType} {action} {type} Length",
        "points": "{reqType} {type} Points"
    },
    "action": {
        "capture": {
            "singular": "Capture",
            "plural": "Captures",
            "short": "Cap"
        },
        "deploy": {
            "singular": "Deploy",
            "plural": "Deploys",
            "short": "Dep"
        },
        "capture_on": {
            "singular": "Capture_on",
            "plural": "Capture_ons",
            "short": "Capon"
        },
        "total": {
            "singular": "Total",
            "plural": "Totals",
            "short": "Tot"
        },
        "streak": {
            "singular": "Streak",
            "plural": "Streaks",
            "short": "Streak"
        },
        "point": {
            "singular": "Point",
            "plural": "Points",
            "short": "Points"
        },
        "different_capture": {
            "singular": "Capture",
            "plural": "Captures",
            "short": "Diff"
        }
    },
    "munzee": {
        "jewel": "Jewel",
        "poi": "POI",
        "greenie": "Greenie",
        "different_poi": "Different POI",
        "clan": "Clan Weapon",
        "virtual": "Virtual",
        "zodiac": "Zodiac",
        "leprechaun": "Leprechaun",
        "nomad": "Nomad",
        "yeti": "Yeti",
        "faun": "Faun",
        "retiremyth": "RetireMyth",
        "mermaid": "Mermaid",
        "hydra": "Hydra",
        "pouch_creature": "Pouch Creature"
    },
    "reqType": {
        "user": "User",
        "clan": "Clan Total"
    },
    "other": {
        "level": "Level",
        "level_n": "Level {level}",
        "level_short": "Lvl",
        "requirements": "Requirements",
        "reloaded_at": "Reloaded at {time} MHQ",
        "reloaded_and_ranking": "Ranking: {rank}/{total} {seperator} Reloaded at {time} MHQ",
        "user_level": "{user} is at Level {level}",
        "reload": "Reload",
        "remove": "Remove",
        "screenshot": "Screenshot",
        "players": "Players",
        "close": "Close",
        "next_month": "Next Month",
        "rewards": "Rewards",
        "reward": "Reward",
        "next_month_rewards": "Next Month Rewards",
        "summary": "Summary"
    }
}
```